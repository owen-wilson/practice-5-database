--EJEMPLO 1


CREATE FUNCTION f_promedio
(@valor1 float,
@valor2 float
)
returns float
as
begin
declare @resultado float
set @resultado = (@valor1 + @valor2) /2
return @resultado
end

SELECT dbo.f_promedio(12,13)


--EJEMPLO 2

CREATE FUNCTION f_codigo
(
@comprobante varchar(5)
)
returns varchar(10)
as
begin
declare @resultado varchar(10)
set @resultado = (case len(@comprobante) when 1 then '0000' + @comprobante
                                         when 2 then '000' + @comprobante
                                         when 3 then '00' + @comprobante
                                         when 4 then '0' + @comprobante
end)
return @resultado
end

SELECT dbo.f_codigo('1')
SELECT dbo.f_codigo('12')
SELECT dbo.f_codigo('456')
SELECT dbo.f_codigo('4565')



--EJEMPLO 3


CREATE FUNCTION f_nombreMes
(@fecha datetime='1992/01/01')
returns varchar(10)
as
begin
declare @nombre varchar(10)
set @nombre = case datename(month,@fecha)
       when 'January' then 'Enero'
       when 'February' then 'Febrero'
       when 'March' then 'Marzo'
       when 'April' then 'Abril'
       when 'May' then 'Mayo'
       when 'June' then 'Junio'
       when 'July' then 'Julio'
       when 'August' then 'Agosto'
       when 'September' then 'Setiembre'
       when 'October' then 'Octubre'
       when 'November' then 'Noviembre'
       when 'December' then 'Diciembre'
     end
    return @nombre
 end;
 
 SELECT dbo.f_nombreMes(1992/01/01)
 SELECT * FROM persona
 
SELECT nombre,
dbo.f_nombreMes(1992/02/02) as 'nacido en el anio'
FROM persona;
