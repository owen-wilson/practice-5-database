CREATE SCHEMA dbo;

CREATE TABLE persona ( 
	persona_id           smallint NOT NULL   ,
	nombre               varchar(50) NOT NULL   ,
	apellido             varchar(50)    ,
	genero               char(1) NOT NULL   ,
	fechaNacimiento      date    ,
	pais                 varchar(20)    ,
	CONSTRAINT Pk_persona_persona_id PRIMARY KEY  ( persona_id )
 );

CREATE TABLE libro ( 
	libro_id             smallint NOT NULL   ,
	titulo               varchar(50) NOT NULL   ,
	precio               decimal(5,2)    ,
	persona_id           smallint NOT NULL   ,
	CONSTRAINT Pk_libro_libro_id PRIMARY KEY  ( libro_id )
 );

CREATE  INDEX Idx_libro_persona_id ON libro ( persona_id );

CREATE FUNCTION f_codigo
(
@comprobante varchar(5)
)
returns varchar(10)
as
begin
declare @resultado varchar(10)
set @resultado = (case len(@comprobante) when 1 then '0000' + @comprobante
                                         when 2 then '000' + @comprobante
                                         when 3 then '00' + @comprobante
                                         when 4 then '0' + @comprobante
end)
return @resultado
end

CREATE FUNCTION f_nombreMes
(@fecha datetime='1992/01/01')
returns varchar(10)
as
begin
declare @nombre varchar(10)
set @nombre = case datename(month,@fecha)
       when 'January' then 'Enero'
       when 'February' then 'Febrero'
       when 'March' then 'Marzo'
       when 'April' then 'Abril'
       when 'May' then 'Mayo'
       when 'June' then 'Junio'
       when 'July' then 'Julio'
       when 'August' then 'Agosto'
       when 'September' then 'Setiembre'
       when 'October' then 'Octubre'
       when 'November' then 'Noviembre'
       when 'December' then 'Diciembre'
     end
    return @nombre
 end;

CREATE FUNCTION f_promedio
(@valor1 float,
@valor2 float
)
returns float
as
begin
declare @resultado float
set @resultado = (@valor1 + @valor2) /2
return @resultado
end

ALTER TABLE libro ADD CONSTRAINT fk_libro_persona FOREIGN KEY ( persona_id ) REFERENCES persona( persona_id );

